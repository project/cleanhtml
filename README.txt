Cleanup bad WYSISYG HTML

This module started as a way to get rid of all those gratuitous non-break spaces
that WYSIWIG editors (such as CKEditor) lob into composed text seemingly at random.
I looked at various solutions, including making them visible in the editor,
but all seemed pretty tortuous, so I decided on the simple solution of just
replacing all nbsp entities with white space.

It was originally just a hook on saving field data, but then I thought
it would also be useful as an output filter, so I've built it that way.
Unusually for a filter it includes the option to have it act on saving data
in addition or instead of acting on display. The advantage of cleaning up
on saving means that you get clean HTML back when editing the node.
Action on saving only applies to fields in nodes and comments,
so not necessarily every use of long-text widgets with HTML formats
(e.g. the signature block in user accounts).

It grew various other options, including stripping out empty tags (recursively),
and the option of leaving a non-break space if it's the only thing bracketed by a tag
(e.g. <p>&nbsp;</p>).
It can also reduce multiple white space to a single space, and remove excess blank lines.

To deal with the case of actually wanting to output a non-break space, there is
an additional simple output filter that converts an underscore to a nbsp entity.
If you do need to output an actual underscore, you can put a <u> tag around an
underscore in the source. This results in an underlined non-break space on output.

These features are separately configurable in the filter settings,
and you can of course use different settings for different text formats.

When ordering the filters, the main Cleanup bad WYSIWYG HTML filter should be
at or near the top of the list, and Non-break underscore at or near the bottom.

Install in the usual way, then go to Configuration->Text formats and
edit the format where you want to apply the filter.
The filter-specific options appear in the appropriate configuration vertical tab. 
